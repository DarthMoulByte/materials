# BGEz Materials
Easy custom shaders for the BGE.

## Getting started
You can take a look at a [video demonstration](https://www.youtube.com/watch?v=cRXFlVro75w), or add your own materials.

Here's the documentation on BGE's [shader objects](https://docs.blender.org/api/blender_python_api_current/bge.types.BL_Shader.html).

### Silly way
```python
from materials import ShaderMaterial

class Silly(ShaderMaterial):

    FragmentShader = '''
uniform float color;

void main()
{
    // OpenGL glsl code
    gl_FragColor = vec4(color, color, color, 1.);
}
    '''

    VertexShader = '''
void main()
{
    // OpenGL glsl code
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_Position = ftransform();

}
    '''

    def init(self):
        # you have access to the object owning the shader:
        print(self.owner.name)

    def pre_draw_setup(self, shader):
        # do stuff here if you need
        shader.setUniform1f('color', 1.)

    def pre_draw(self, shader):
        # do stuff here if you need

    def post_draw(self, shader):
        # do stuff here if you need
```

### Slightly better way
```python
from materials import ShaderMaterial
from materials import ReadFile

class LessSilly(ShaderMaterial):
    FragmentShader = ReadFile('yourFragmentShader.fs', __file__)
    VertexShader = ReadFile('yourVertexShader.fs', __file__)

    def init(self):
        self.param = 123

    def pre_draw_setup(self, shader):
        # do stuff here if you need

    def pre_draw(self, shader):
        # do stuff here if you need

    def post_draw(self, shader):
        # do stuff here if you need
```

### Apply the shader
Simply call the object you created from a Python controller in module mode.
