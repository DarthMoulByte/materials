import bge

__all__ = ['GameObjectReference']

class GameObjectReference(object):
    '''This object should keep track of a KX_GameObject, this until the death
    of the refered instance.'''

    def __init__(self, object):
        self._reference = object
        object[id(self)] = True

    @property
    def object(self):
        if not self._reference.invalid:
            return self._reference

        for scene in bge.logic.getSceneList():
            for object in scene.objects:
                if object.get(id(self), False):
                    self._reference = object
                    return object

        return None
